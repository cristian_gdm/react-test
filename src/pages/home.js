import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

class Home extends Component {
    state = {
        data: [],
        sortedData: [],
    }

    componentDidMount() {
        this.setData();
    }

    setData = () => {
        // localStorage.clear();
        console.log(localStorage);

        let data = localStorage.getItem('data');
        if (data) {
            let updatedData = [];

            const data = localStorage.getItem('data');
            const parsedData = JSON.parse(data);

            if (parsedData) {
                parsedData.forEach(el => updatedData.push(el));
                this.setState({ data: updatedData, sortedData: updatedData });
            }

            // store
            if (!this.props.storeData.length) {
                console.log('store get data');
                this.props.onGetData(updatedData);
            }
        }
    }

    handleSort = () => {
        let [...updatedData] = this.state.sortedData;
        updatedData = updatedData.sort((a, b) => (a.email > b.email) ? 1 : -1);

        this.setState({ sortedData: updatedData });
    }

    handleReset = () => {
        this.setState({ sortedData: this.state.data });
    }

    render() {
        const { sortedData } = this.state;

        return (
            <div>
                <h2>Home</h2>

                <Button variant="contained" onClick={this.handleSort}>Sort</Button>
                <Button variant="contained" onClick={this.handleReset}>Clear</Button>

                {sortedData.length
                    ? <table>
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Year</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sortedData.map((el, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{el.email}</td>
                                        <td>{el.year}</td>
                                    </tr>

                                )
                            })}
                        </tbody>
                    </table>
                    : <p>No data!</p>
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        storeData: state.data
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetData: (data) => dispatch({ type: 'GET_DATA', payload: data })
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);