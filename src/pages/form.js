import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { Button } from '@material-ui/core';
import { connect } from 'react-redux';

class Form extends Component {
    state = {
        email: '',
        year: '',
        isLoggedIn: false,
        hasLoaded: false,
        sent: false
    }

    componentDidMount() {
        this.setData();
    }

    setData = () => {
        let isLoggedIn = localStorage.getItem('isLoggedIn');
        if (isLoggedIn) {
            this.setState({ isLoggedIn: true });
        }
        this.setState({ hasLoaded: true });
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleSubmit = () => {
        let formData = {
            email: this.state.email,
            year: this.state.year
        }

        // localStorage
        // let updatedData = [];
        // const data = localStorage.getItem('data');
        // const parsedData = JSON.parse(data);

        // if (parsedData) {
        //     parsedData.forEach(el => updatedData.push(el));
        //     updatedData = [...parsedData, formData];
        // } else {
        //     updatedData = [formData];
        // }

        // localStorage.setItem('data', JSON.stringify(updatedData));
        // this.setState({ sent: true });

        // store
        console.log('store add data');
        this.props.onAddData(formData);

        localStorage.setItem('data', JSON.stringify(this.props.storeData));
        this.setState({ sent: true });
    }

    render() {
        const { email, year, isLoggedIn, hasLoaded, sent } = this.state;

        return (
            <div>
                {sent &&
                    <Redirect to='/' />
                }

                <h2>Form</h2>
                {!isLoggedIn && hasLoaded
                    ? <Redirect to='/login' />
                    : <ValidatorForm
                        ref="form"
                        onSubmit={this.handleSubmit}
                    >
                        <TextValidator
                            label="Email"
                            onChange={this.handleChange}
                            name="email"
                            value={email}
                            validators={['required', 'isEmail']}
                            errorMessages={['This field is required', 'Email is not valid']}
                        />
                        <br />
                        <TextValidator
                            label="Year"
                            onChange={this.handleChange}
                            name="year"
                            value={year}
                            validators={['required', 'isNumber', 'minNumber:1900', 'maxNumber:2002']}
                            errorMessages={['This field is required', 'Not number', 'Too old', 'Too young']}
                        />
                        <br />
                        <Button
                            variant="contained"
                            type="submit"
                        >
                            Submit
                        </Button>
                    </ValidatorForm>
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        storeData: state.data
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddData: (data) => dispatch({ type: 'ADD_DATA', payload: data })
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);