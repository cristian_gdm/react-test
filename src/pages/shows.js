import React, { Component } from 'react';
import axios from 'axios';
import { Button } from '@material-ui/core';

class Shows extends Component {
    state = {
        shows: [],
        sortedShows: [],
        loading: true,
        page: 1,
    }

    componentDidMount() {
        this.getData();
    }

    getData = () => {
        axios.get('https://api.tvmaze.com/shows')
            .then(res => {
                this.setState({ shows: res.data });
                this.sortData(1);
                this.setState({loading: false});
            })
    }

    sortData = (page) => {
        let [...updatedData] = this.state.shows;
        updatedData = updatedData.slice((page - 1) * 50, page * 50);
        this.setState({ sortedShows: updatedData });
    }

    handlePrev = () => {
        this.sortData(this.state.page - 1);
        this.setState({page: this.state.page - 1});
    }


    handleNext = () => {
        this.sortData(this.state.page + 1);
        this.setState({page: this.state.page + 1});
    }

    render() {
        const { sortedShows, page, loading } = this.state;

        return (
            <div>
                <h2>Shows</h2>
                <Button variant="contained" onClick={this.handlePrev} disabled={page === 1}>Prev</Button>
                <Button variant="contained" onClick={this.handleNext} disabled={page === 5}>Next</Button>
                {!loading && sortedShows.length
                    ? <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Language</th>
                                <th>Rating</th>
                                <th>Image</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sortedShows.map((el, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{el.name}</td>
                                        <td>{el.language}</td>
                                        <td>{el.rating.average}</td>
                                        <td>{el.image.original}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    : <div>Loading...</div>
                }
            </div>
        )
    }
}

export default Shows;