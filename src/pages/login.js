import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { Button } from '@material-ui/core';

class Login extends Component {
    state = {
        login: false
    }

    handleLogin = () => {
        localStorage.setItem('isLoggedIn', true);
        this.setState({ login: true });
    }

    render() {
        const { login } = this.state;

        return (
            <div>
                <h2>Login</h2>
                {login
                    ? <Redirect to='/form' />
                    : <Button variant="contained" onClick={this.handleLogin}>Login</Button>
                }
            </div>
        )
    }
}

export default Login;