import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { Button } from '@material-ui/core';

class Logout extends Component {
    state = {
        logout: false
    }

    handleLogout = () => {
        localStorage.removeItem('isLoggedIn')
        this.setState({ logout: true });
    }

    render() {
        const { logout } = this.state;

        return (
            <div>
                <h2>Logout</h2>
                {logout
                    ? <Redirect to='/' />
                    : <Button variant="contained" onClick={this.handleLogout}>Logout</Button>
                }
            </div>
        )
    }
}

export default Logout;