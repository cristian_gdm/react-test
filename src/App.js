import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import HomePage from "./pages/home";
import LoginPage from "./pages/login";
import FormPage from "./pages/form";
import LogoutPage from "./pages/logout";
import ShowsPage from "./pages/shows";

function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/login">Login</Link>
              </li>
              <li>
                <Link to="/form">Form</Link>
              </li>
              <li>
                <Link to="/logout">Logout</Link>
              </li>
              <li>
                <Link to="/shows">Shows</Link>
              </li>
            </ul>
          </nav>
          
          <hr />

          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route exact path="/login">
              <LoginPage />
            </Route>
            <Route exact path="/form">
              <FormPage />
            </Route>
            <Route exact path="/logout">
              <LogoutPage />
            </Route>
            <Route exact path="/shows">
              <ShowsPage />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
