const initialState = {
    data: []
}

const reducer = (state = initialState, action) => {
    if (action.type === 'GET_DATA') {
        return { data: action.payload }
    }
    if (action.type === 'ADD_DATA') {
        return { data: [...state.data, action.payload] }
    }
    return state;
};

export default reducer;